FROM tomcat:8.0
RUN cp -R /usr/local/tomcat/webapps.dist/.  /usr/local/tomcat/webapps/
COPY webapp.war /usr/local/tomcat/webapps/
